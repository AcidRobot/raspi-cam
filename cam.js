var RaspiCam = require("raspicam");

// var camera = new RaspiCam({
//     mode: "video",
//     w: 640,
//     h: 480,
//     output: "/home/pi/node_programs/amp-drone/raspi-cam/new.h264",
//     timeout: 10000,
//     framerate: 30
//     });
var camera = new RaspiCam({
    mode: "timelapse",
    w: 640,
    h: 480,
    output: "/home/pi/node_programs/amp-drone/raspi-cam/robot.jpg",
    timeout: 10000,
    timelapse: 100,
    encoding: "jpg"
    });
//to take a snapshot, start a timelapse or video recording
var process_id = camera.start({});
console.log(process_id);
//to stop a timelapse or video recording
// camera.stop();
