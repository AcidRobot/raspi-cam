var controller = require('http').createServer(handler);
var io = require('socket.io').listen(controller);
var fs = require('fs');
var express = require('express'),
app = express();//,
// port = process.env.PORT || 4000;
var RaspiCam = require("raspicam");

var camera = new RaspiCam({
    mode: "timelapse",
    w: 640,
    h: 480,
    output: "/home/pi/node_programs/amp-drone/raspi-cam/public/robot.jpg",
    timeout: 15000,
    timelapse: 100,
    encoding: "jpg"
    });

app.use('/static', function(req, res, next) {
        req.url = req.url.replace(/\/([^\/]+)\.[0-9a-f]+\.(css|js|jpg|png|gif|svg)$/, "/$1.$2");
        next();
});

app.use('/static', express.static('/home/pi/node_programs/amp-drone/raspi-cam/public', {maxAge: 30}));

var server = app.listen(3000, function () {

  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);

});

controller.listen(8080);

function handler (req, res) {
   fs.readFile(__dirname + '/index.html',
    function (err, data) {
      if (err) {
        res.writeHead(500);
        return res.end('Error loading index.html');
      }
      console.log('Connection');
      res.writeHead(200);
      res.end(data);
      }
   );
}
// while(1==1){
var process_id = camera.start({});

console.log(process_id);
// }
